## How to add touchpad gestures on Arch Linux and KDE Plasma

Following this very old tutorial I wanted to see if I could create a touchpad gesture on Arch Linux and KDE Plasma 6.
https://www.lorenzobettini.it/2017/02/touchpad-gestures-in-linux-kde-with-libinput-gestures/

My goal was to have a similar gesture to macOS's three finger swipe up to open so-called Mission Control, just like the application overview on Plasma (default shortcut is Meta+W).

1. Install the `libinput-gestures` package from the AUR: https://aur.archlinux.org/packages/libinput-gestures
    (I use yay for that.  
    `# yay -S libinput-gesture`
2. Install xdotool to bind gestures to virtual key strokes and/or optionally wmctrl for libinput-gestures internal commands.
    `# pacman -S xdotool wmctrl`
3. Add the current user to the input group:
    `# gpasswd -a $USER input`
4. Start libinput-gestures and make it autostart at login:
    $ `libinput-gestures-setup autostart start`
5. Copy the default config file to your $HOME.
    `# cp /etc/libinput-gestures.conf ~/.config/libinput-gestures.conf`
6. Edit the file and add a gesture. For example the following:
   `gesture swipe up 3 xdotool key super+w`
7. Restart libinput-gestures:
    $ `libinput-gestures-setup restart`
8. Now when swiping up on your touchpad using three finger, xdotool is pressing the Super- or Meta-Key and "W", whih opens the application overview.