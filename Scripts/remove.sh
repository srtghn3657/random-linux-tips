# Script to easily remove packages without having to spell the whole package-name.

# Create the following alias to make removing packages even easier:
# alias remove="sh /path/to/remove.sh"

# Now you can remove packages by using the following command:
# remove $packagename

#!/bin/bash

# Query installed packages for the search term and create a variable from it
pkgs=( $(pacman -Qq | grep $1) )

# Make it an array
declare -a pkgs

# If array contains only one entry, remove immediately
if [ ${#pkgs[@]} -eq 1 ]; then
    # If installed, use yay to uninstall the selected package and all of it's dependecies and configuration files
    if type yay >/dev/null 2>&1; then
        yay -Rns $pkgs
    else
        # Else use pacman to uninstall the selected package and all of it's dependecies and configuration files
        sudo pacman -Rns $pkgs
    fi
else
    # Else select the right package from the array
    select pkg in ${pkgs[@]}
    do
    break
    done

    # If installed, use yay to uninstall the selected package and all of it's dependecies and configuration files
    if type yay >/dev/null 2>&1; then
        yay -Rns $pkgs
    else
        # Else use pacman to uninstall the selected package and all of it's dependecies and configuration files
        sudo pacman -Rns $pkgs
    fi
fi