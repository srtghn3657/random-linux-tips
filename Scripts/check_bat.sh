#!/bin/bash

# Check if system is charging or not.
# Continue only if charging.

on_power=$(cat /sys/class/power_supply/AC/online)

if [[ $on_power == 0 ]]; then
        echo "Device is not connected to power. Exiting.."
        exit 0
else
        echo "Device is connected to power. Continuing.."
fi
