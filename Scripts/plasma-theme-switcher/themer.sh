#!/bin/bash

# Changes to BreezeDark at 21:30 and to BreezeLight at 6:30.

# Get the current hour and minute
current_hour=$(date +%H)
current_minute=$(date +%M)

# Convert current time to minutes since midnight
current_time=$((10#$current_hour * 60 + 10#$current_minute))

# Define the start times in minutes since midnight
start_command1=$((21 * 60 + 30)) # 21:30 in minutes
end_command1=$((6 * 60 + 30)) # 06:30 in minutes

if (( current_time >= start_command1 || current_time < end_command1 )); then
    # Execute command1 if the time is between 21:30 and 06:30
    plasma-apply-colorscheme BreezeDark
else
    # Execute command2 if the time is between 06:30 and 21:30
    plasma-apply-colorscheme BreezeLight
fi