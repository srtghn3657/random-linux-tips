Moved -> https://gitlab.com/dataprolet/plasma-theme-switcher

# Automatically change Plasma theme between dark and light

1. Place timer and service file in the respective user directory $HOME/.config/systemd/user
2. Place the script into a directory and specify the path in the service file.
3. Change time in script if necessary (default is 9:30 p.m. and 6:00 a.m.)
4. Alternatively use the themer-sun.sh script to change themes according to your locations sunrise/sunset and specify location in the script
5. Enable and start the timer:  
$ systemctl --user enable themer.timer  
$ systemctl --user start themer.timer
6. Optional: add script to autostart to check on login