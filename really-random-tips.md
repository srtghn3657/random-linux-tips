## Most random tips I could think of:

1. Firefox has an about.about section, listing all other about:X pages.
2. Clean your Arch system: `sudo pacman -Qttdq | sudo pacman -Rnsscu -`
3. Ferment lemons in salt.
4. Enable TRIM: `systemctl enable fstrim.timer`
5. https://github.com/ibraheemdev/modern-unix
6. Chatbots: https://www.flowgpt.com/chat; https://perplexity.ai